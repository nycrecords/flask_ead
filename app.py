import os
from flask import Flask, render_template, request, url_for, make_response,redirect, url_for
from jinja2 import Environment, FileSystemLoader
from flask import Flask, request, jsonify
from flask.ext import excel
from werkzeug import secure_filename
from flask import send_from_directory
from pyexcel_xls import XLBook
import json
import xlrd
import logging
import unicodedata

UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = set(['xls','xlsx'])

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/form')
def form():
    return render_template('form_submit.html')

@app.route('/ead/', methods=['POST'])
def ead():
    series=request.form['series']
    level=request.form['level']
    ead_xml = render_template('form_action.html', series=series, level=level)
    response = make_response(ead_xml)
    #return render_template('form_action.html', series=series, level=level)
    response.headers["Content-Type"] = "application/xml"
    return response

@app.route("/getxml", methods=['GET', 'POST'])
def dotransform():
    data = load_data("your_file.xls") 
    return ''' <!doctype html>
    <title>Upload an excel file</title>
    <h1>Excel file upload (xls, xlsx, ods please)</h1>
    <form action="" method=post enctype=multipart/form-data><p>
    <input type=file name=file><input type=submit value=Upload>
    </form>
    '''

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

@app.route('/uploads2/<filename>')
def uploaded_file(filename):
  filePath = str(app.config['UPLOAD_FOLDER']) + "/" + filename
  workbook = xlrd.open_workbook(filePath)
  worksheet = workbook.sheet_by_name('EAD')
  curr_row = 0
  curr_content_row = 1
  num_rows = worksheet.nrows
  xmlStr = "<ead>\n"
  xmlStr += '<eadheader audience="external" countryencoding="iso3166-1" dateencoding="iso8601" langencoding="iso639-2b" repositoryencoding="iso15511" relatedencoding="DC" scriptencoding="iso15924">\n'
  xmlStr += '<eadid countrycode="us" mainagencycode="code">' + worksheet.cell_value(rowx=1,colx=0) + '</eadid>\n'
  xmlStr += "<filedesc>\n";
  xmlStr += "<titlestmt>\n";
  titleproper = '<titleproper encodinganalog="title">\n'
  titleproper += worksheet.cell_value(rowx=1,colx=6)
  tdate = str(worksheet.cell_value(rowx=1,colx=15))
  if "." in tdate:
    tdate = tdate.split(".")[0]
  tdateAttr = tdate
  if ' ' in tdateAttr:
    tdateAttr = tdateAttr.split(" ")[1]
  if '-' in tdateAttr:
    tdateAttr = tdateAttr.split("-")[0] + "/" + tdateAttr.split("-")[1]

  titleproper += '<date normal="' + tdateAttr + '" type="inclusive" encodinganalog="date" certainty="approximate">' + tdate + '</date>'
  titleproper += "</titleproper>"
 
  publisher = "<publisher>" + worksheet.cell_value(rowx=1,colx=13) + "</publisher>"
  address = "<address>" + worksheet.cell_value(rowx=1,colx=14) + "</address>"
  xmlStr += str(titleproper) + "\n"
  xmlStr += '<author encodinganalog="contributor">' + worksheet.cell_value(rowx=1,colx=8) + "</author>"
  xmlStr += "</titlestmt>\n"
  xmlStr += "</filedesc>\n"
  xmlStr += str(publisher) + "\n"
  xmlStr += str(address) + "\n"
  xmlStr += "</eadheader>\n"
 
  archdescDid = workbook.sheet_by_name('Archdesc did')
  xmlStr+= '<archdesc level="collection" type="inventory" relatedencoding="MARC21">\n'
  xmlStr+= "<did><head>" + archdescDid.cell_value(rowx=2,colx=0) + "</head>" + "\n"
  xmlStr+= "<unittitle>" + archdescDid.cell_value(rowx=2,colx=1)
  udate = str(archdescDid.cell_value(rowx=2,colx=2))
  udateAttr = udate
  if ' ' in udateAttr:
    udateAttr = udateAttr.split(" ")[1]
  if '-' in udate:
    udateAttr = udateAttr.split("-")[0] + "/" + udateAttr.split("-")[1]
  xmlStr+= '<unitdate normal="' + udateAttr + '" type="inclusive" encodinganalog="date" certainty="approximate">' + udate + '</unitdate>'
  xmlStr+=  "</unittitle>" + "\n"
  udate = str(archdescDid.cell_value(rowx=2,colx=2))
  #xmlStr+= '<origination label="Creator"><persname encodinganalog="100" source="lcnaf" role="creator">' + worksheet.cell_value(rowx=2,colx=3) + '</persname></origination>' + '\n'
  physdesc = str(archdescDid.cell_value(rowx=2,colx=5))
  if "," in physdesc:
    physdescList = physdesc.split(",")
    xmlStr += '<physdesc label="Extent">\n'
    xmlStr += '<extent encodinganalog="300" unit="box">"' + physdescList[0] + ',</extent>\n'
    xmlStr += '<extent encodinganalog="300" unit="box">"' + physdescList[1] + ',</extent>\n'
    xmlStr += '<extent encodinganalog="300" unit="box">"' + physdescList[2] + ',</extent>\n'
    xmlStr += '</physdesc>\n'
  xmlStr += '<langmaterial label="Language of Material" encodinganalog="546">\n'
  xmlStr += 'Collection material in\n';
  xmlStr += '<language encodinganalog="041" langcode="eng">' + archdescDid.cell_value(rowx=2,colx=8) + '</language>\n'
  xmlStr += '</langmaterial>\n'
  xmlStr += '<repository label="Repository" encodinganalog="852">\n'
  xmlStr += '<corpname>' + archdescDid.cell_value(rowx=2,colx=9) + '</corpname>\n'
  xmlStr += '</repository>\n'
  xmlStr += '</did>\n'
  
  #archdescAdmin = workbook.sheet_by_name('Archdesc admininfo')
  #xmlStr += '<descgrp type="admininfo">\n'
  #xmlStr += '<prefercite encodinganalog="524">' + archdescAdmin.cell_value(rowx=1,colx=0) + '</prefercite>' + "\n"
  #xmlStr += '<accessrestrict encodinganalog="506">' + archdescAdmin.cell_value(rowx=1,colx=3) + '</accessrestrict>' + "\n"
  #xmlStr += '<userestrict encodinganalog="540">' + archdescAdmin.cell_value(rowx=1,colx=4) + '</userestrict>' + "\n"
  #xmlStr += '<processinfo encodinganalog="583">' + archdescAdmin.cell_value(rowx=1,colx=6) + '</processinfo>' + "\n"
  #xmlStr += '<accruals encodinganalog="584">' + archdescAdmin.cell_value(rowx=1,colx=7) + '</accruals>' + "\n"
  #xmlStr += '</descgrp>\n'

  archdescBioHist = workbook.sheet_by_name('Archdesc bioghist')
  #xmlStr += '<bioghist encodinganalog="545">\n'
  #xmlStr += archdescBioHist.cell_value(rowx=1,colx=0) + '\n'
  #xmlStr += archdescBioHist.cell_value(rowx=1,colx=1) + '\n'
  #xmlStr += '</bioghist>\n'

  archdescScopeContent = workbook.sheet_by_name('Archdesc scopecontent')
  #xmlStr += '<scopecontent encodinganalog="520">\n'
  #xmlStr += archdescScopeContent.cell_value(rowx=1,colx=0) + '\n'
  #xmlStr += '</scopecontent>\n'

  archdescArrangement = workbook.sheet_by_name('Archdesc Arrangement')
  xmlStr += '<arrangement encodinganalog="351">\n'
  xmlStr += '<list type="simple">\n'
  #xmlStr += archdescArrangement.cell_value(rowx=1,colx=0) + '\n'
  for n in range(1, archdescArrangement.nrows):
    item = archdescArrangement.cell_value(rowx=n,colx=2)
    target = item.split(" ")[0] + item.split(" ")[1]
    target = target.lower().replace(".","")
    if "series" in target:
      target = "series" + target.split("series")[1].replace("vi","6").replace("vi","6").replace("v","5").replace("iv","4").replace("iii","3").replace("ii","2").replace("i","1")
    item = item.replace("SubGroup","").replace("Series","")
    xmlStr += '<item>\n'
    xmlStr += '<ref target="' + target + '" actuate="onrequest" show="replace">' + item + '</ref>' + '\n'
    xmlStr += '</item>\n'
  xmlStr += '</list>\n'
  xmlStr += '</arrangement>\n'

  xmlStr += '</archdesc>\n'

  dscContainerList = workbook.sheet_by_name('dsc container list')
  xmlStr += '<dsc type="in-depth">\n'
  xmlStr += '<head>Container List</head>\n'
  ncols = dscContainerList.ncols - 1
  nrows = dscContainerList.nrows - 1
  #xmlStr += str(dscContainerList.ncols)
  #xmlStr += "<did>" + ncols +  "," + nrows + "</did>\n"
  for curr_row in range(2, dscContainerList.nrows):
    containerNonEmpty = False
    container_cell_type = dscContainerList.cell_type(curr_row, 15)
    #app.logger.info(str(dscContainerList.cell_value(curr_row, 15)))
    if container_cell_type != 0 and container_cell_type != 6:
      containerNonEmpty = True
      container_one_type = dscContainerList.cell_value(curr_row, 15)
      container_two_type = dscContainerList.cell_value(curr_row, 22)
      container_one_type_unit_title_type = dscContainerList.cell_type(curr_row, 17)
      container_series_type = dscContainerList.cell_type(curr_row, 0)
      container_series_value = str(dscContainerList.cell_value(curr_row, 0))
      if container_one_type_unit_title_type != 0 and container_one_type_unit_title_type != 6:
        container_one_type_unit_title = str(dscContainerList.cell_value(curr_row, 17))
        if len(container_one_type_unit_title) > 1:
          container_one_type = str(dscContainerList.cell_value(curr_row, 15))
          container_one_type_number = str(dscContainerList.cell_value(curr_row, 16))
          xmlStr += '<c01 level="file">\n'
          if dscContainerList.cell_type(curr_row,19) != 0 and dscContainerList.cell_type(curr_row,19) != 6:
            physdesc = str(dscContainerList.cell_value(curr_row,19))
            if "," in physdesc:
              physdescList = physdesc.split(",")
              xmlStr += '<physdesc label="Extent">\n'
              for p in physdescList:
                xmlStr += '<extent encodinganalog="300" unit="box">"' + p + ',</extent>\n'
              if dscContainerList.cell_type(curr_row,28) != 0 and dscContainerList.cell_type(curr_row,28) != 6:
                xmlStr += '<genreform>' + dscContainerList.cell_value(curr_row,28) + '</genreform>\n'
              xmlStr += '</physdesc>\n'
          if dscContainerList.cell_type(curr_row,27) != 0 and dscContainerList.cell_type(curr_row,27) != 6:
            phystech = str(dscContainerList.cell_value(curr_row,27))
            xmlStr += '<phystech>\n'
            xmlStr += '<head>Physical Characteristics</head>\n'
            xmlStr += '<p>' + phystech + '</p>\n'
            xmlStr += '</phystech>\n'
          xmlStr += '<did>\n'
          xmlStr += '<container type="' + container_one_type.lower() + '">' + container_one_type_number + '</container>\n'
          xmlStr += '<unittitle>' + container_one_type_unit_title + '</unittitle>\n'
          if dscContainerList.cell_type(curr_row,29) != 0 and dscContainerList.cell_type(curr_row,29) != 6:
            note = str(dscContainerList.cell_value(curr_row,29))
            xmlStr += '<note>\n'
            xmlStr += '<p>' + note + '</p>\n'
            xmlStr += '</note>\n'
          if dscContainerList.cell_type(curr_row,35) != 0 and dscContainerList.cell_type(curr_row,35) != 6:
            physloc = str(dscContainerList.cell_value(curr_row,35))
            xmlStr += '<physloc>\n'
            xmlStr +=  physloc + '\n'
            xmlStr += '</physloc>\n'
          xmlStr += '</did>\n'
          xmlStr += '</c01>\n'
      if container_series_type != 0 and container_series_type != 6:
        xmlStr += '<c01 level="series" tpattern="container:description">\n'
        xmlStr += '<scopecontent><p>' + dscContainerList.cell_value(curr_row, 3) + '</p></scopecontent>\n'
        relatedMaterialCellType = dscContainerList.cell_type(curr_row, 5)
        if relatedMaterialCellType != 0 and relatedMaterialCellType != 6:
          xmlStr += '<relatedmaterial>\n'
          xmlStr += '<ref>' + dscContainerList.cell_value(curr_row, 5) + '</ref>\n'
          xmlStr += '</relatedmaterial>\n'
        xmlStr += '<did>\n'
        udate = str(dscContainerList.cell_value(curr_row,2))
        udateAttr = udate
        if ' ' in udateAttr:
          udateAttr = udateAttr.split(" ")[1]
        if '-' in udate:
          udateAttr = udateAttr.split("-")[0] + "/" + udateAttr.split("-")[1]
        xmlStr += '<unitid id="series' + container_series_value.translate(None,'.').lower() + '">' + container_series_value + '</unitid>\n'
        xmlStr += '<unittitle>' + dscContainerList.cell_value(curr_row, 1)  + ','
        xmlStr += '<unitdate normal="' + udateAttr + '" type="inclusive" encodinganalog="date">' + udate + '</unitdate>\n'
        xmlStr += '</unittitle>\n'
        if dscContainerList.cell_type(curr_row,19) != 0 and dscContainerList.cell_type(curr_row,19) != 6:
          physdesc = str(dscContainerList.cell_value(curr_row,19))
          if "," in physdesc:
            physdescList = physdesc.split(",")
            xmlStr += '<physdesc label="Extent">\n'
            for p in physdescList:
              xmlStr += '<extent encodinganalog="300" unit="box">"' + p + ',</extent>\n'
            if dscContainerList.cell_type(curr_row,28) != 0 and dscContainerList.cell_type(curr_row,28) != 6:
                xmlStr += '<genreform>' + dscContainerList.cell_value(curr_row,28) + '</genreform>\n'
            xmlStr += '</physdesc>\n'
        if dscContainerList.cell_type(curr_row,27) != 0 and dscContainerList.cell_type(curr_row,27) != 6:
          phystech = str(dscContainerList.cell_value(curr_row,27))
          xmlStr += '<phystech>\n'
          xmlStr += '<head>Physical Characteristics</head>\n'
          xmlStr += '<p>"' + phystech + '</p>\n'
          xmlStr += '</phystech>\n'
        if dscContainerList.cell_type(curr_row,29) != 0 and dscContainerList.cell_type(curr_row,29) != 6:
            note = str(dscContainerList.cell_value(curr_row,29))
            xmlStr += '<note>\n'
            xmlStr += '<p>"' + note + '</p>\n'
            xmlStr += '</note>\n'
        if dscContainerList.cell_type(curr_row,35) != 0 and dscContainerList.cell_type(curr_row,35) != 6:
            physloc = str(dscContainerList.cell_value(curr_row,35))
            xmlStr += '<physloc>\n'
            xmlStr += physloc + '\n'
            xmlStr += '</physloc>\n'
        xmlStr += '</did>\n'
      else:
        if container_two_type == "Folder":
          udate = str(dscContainerList.cell_value(curr_row,25))
          udateAttr = udate
          if ' ' in udateAttr:
            udateAttr = udateAttr.split(" ")[1]
          if '-' in udate:
            udateAttr = udateAttr.split("-")[0] + "/" + udateAttr.split("-")[1]
          xmlStr += '<c02 level="subseries" tpattern="container:description">\n'
          xmlStr += '<did>\n'
          container_two_type_number = str(dscContainerList.cell_value(curr_row, 16))
          container_two_type_folder = str(dscContainerList.cell_value(curr_row,23))
          container_above = str(dscContainerList.cell_value(curr_row-1, 16))
          container_subseries = str(dscContainerList.cell_value(curr_row, 6))
          xmlStr += '<unitid id="series' + container_subseries.translate(None,'.').lower() + '">' + container_subseries + '</unitid>\n'
          xmlStr += '<container type="' + container_one_type.lower() + '">' + container_two_type_number + '</container>\n'
          xmlStr += '<container type="folder">' + container_two_type_folder + '</container>\n'
          xmlStr += '<unittitle>' + dscContainerList.cell_value(curr_row, 24)  + ','
          xmlStr += '<unitdate normal="' + udateAttr + '" type="inclusive" encodinganalog="date">' + udate + '</unitdate>\n'
          xmlStr += '</unittitle>\n'
          if dscContainerList.cell_type(curr_row,19) != 0 and dscContainerList.cell_type(curr_row,19) != 6:
            physdesc = str(dscContainerList.cell_value(curr_row,19))
            if "," in physdesc:
              physdescList = physdesc.split(",")
              xmlStr += '<physdesc label="Extent">\n'
              if dscContainerList.cell_type(curr_row,28) != 0 and dscContainerList.cell_type(curr_row,28) != 6:
                xmlStr += '<genreform>' + dscContainerList.cell_value(curr_row,28) + '</genreform>\n'
              for p in physdescList:
                xmlStr += '<extent encodinganalog="300" unit="box">"' + p + ',</extent>\n'
              xmlStr += '</physdesc>\n'
          if dscContainerList.cell_type(curr_row,27) != 0 and dscContainerList.cell_type(curr_row,27) != 6:
            phystech = str(dscContainerList.cell_value(curr_row,27))
            xmlStr += '<phystech>\n'
            xmlStr += '<head>Physical Characteristics</head>\n'
            xmlStr += '<p>"' + phystech + '</p>\n'
            xmlStr += '</phystech>\n'
          if dscContainerList.cell_type(curr_row,29) != 0 and dscContainerList.cell_type(curr_row,29) != 6:
            note = str(dscContainerList.cell_value(curr_row,29))
            xmlStr += '<note>\n'
            xmlStr += '<p>"' + note + '</p>\n'
            xmlStr += '</note>\n'
          if dscContainerList.cell_type(curr_row,35) != 0 and dscContainerList.cell_type(curr_row,35) != 6:
            physloc = str(dscContainerList.cell_value(curr_row,35))
            xmlStr += '<physloc>\n'
            xmlStr += physloc + '\n'
            xmlStr += '</physloc>\n'
          xmlStr += '</did>\n'
          xmlStr += '</c02>\n'
          if container_above != container_two_type_folder:
            xmlStr += '</c01>\n'
          #app.logger.info(str(curr_col))
      #app.logger.info(str(curr_row) + "," + str(curr_col))
    if (curr_row + 1) < dscContainerList.nrows:
      container_val = str(dscContainerList.cell_value(curr_row, 15))
      container_one_down = str(dscContainerList.cell_value(curr_row + 1, 15))
      
      if xmlStr[-5:] != 'c01>\n' and len(container_val) >= 1 and len(container_one_down) < 1:
        #app.logger.info(xmlStr[-5:])
        xmlStr += '</c01>\n'
  xmlStr = xmlStr.replace('</did>\n<c01>','</did>\n</c01>\n<c01>').replace('</did>\n<c01 level','</did>\n</c01>\n<c01 level').replace('</c02>\n<c01','</c02>\n</c01>\n<c01')
  curr_row = -1
  while curr_row < nrows:
    curr_row += 1
    row = dscContainerList.row(curr_row)
    curr_cell = -1
    #xmlStr += '<did>\n'
    while curr_cell < ncols:
      curr_cell += 1
      cell_type = dscContainerList.cell_type(curr_row, curr_cell)
      if cell_type != 0 and cell_type != 6:
        #xmlStr += "<rowCol>" + str(curr_row) + "," + str(curr_cell) + "</rowCol>\n"
        content = dscContainerList.cell_value(curr_row, curr_cell)
        if cell_type == 1:
          content = content.encode('ascii','ignore')
        if curr_row >= 2:
          pass 
          #xmlStr += '<content>' + str(content)+ '</content>\n'
          #if curr_cell == 0:
          #  xmlStr += '<unitid id="series1">' + str(content) + '</unitid>\n' 
    #xmlStr += '</did>\n'
    #unitId = dscContainerList.cell_value(rowx=n,colx=1)
    #xmlStr += '<unitid id="series1">' + unitId + '</unitid>\n'
    #xmlStr += '</did>\n'
  xmlStr += '</dsc>\n'
  xmlStr += '</ead>\n'
  response = make_response(xmlStr)
  response.headers["Content-Type"] = "application/xml"
  return response


@app.route('/uploads/<filename>')
def prev_uploaded_file(filename):
    #return send_from_directory(app.config['UPLOAD_FOLDER'],
    #                           filename)
    filePath = str(app.config['UPLOAD_FOLDER']) + "/" + filename
    print filePath
    book = XLBook(filePath)
    sheets = book.sheets()
    #print sheets
    dscStr = ""
    sheetList = sheets['EAD Skeleton to Copy']
    #for c in sheetList:
    #   xmlStr = "".join(c)
    #   if(xmlStr.startswith("<")):
    #      dscStr += "".join(c)
    containerSheet = sheets['EAD Result to Copy']
    for c in containerSheet:
       xmlStr = "".join(c)
       if(xmlStr.startswith("<")):
          dscStr += "".join(c)     
    ead_xml = render_template('ead.xml', containerList=dscStr)
    response= make_response(ead_xml)
    response.headers["Content-Type"] = "application/xml"
    return response
    return'''
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ead xsi:schemaLocation="urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns2="http://www.w3.org/1999/xlink" xmlns="urn:isbn:1-931666-22-9">
<eadheader repositoryencoding="iso15511" countryencoding="iso3166-1" dateencoding="iso8601" langencoding="iso639-2b">
<eadid></eadid>
<filedesc>
<titlestmt>
<titleproper>
<num>1111.Test_Collection1</num>
</titleproper>
</titlestmt>
<publicationstmt>
<publisher>Archivists' Toolkit</publisher>
</publicationstmt>
</filedesc>
<profiledesc>
<langusage>English</langusage>
</profiledesc>
</eadheader>
<archdesc level="collection">
<did>
<unittitle>Import of Test_Collection1</unittitle>
<unitid>1111.Test_Collection1</unitid>
<repository>
<corpname>Archivists' Toolkit</corpname>
</repository>
<langmaterial>
<language langcode="eng"/>
</langmaterial>
<physdesc>
<extent>12345 Linear feet</extent>
</physdesc>
<unitdate>1999</unitdate>
</did>

<dsc>

{containerList}

</dsc>

</archdesc>
</ead>
'''.format(containerList=toCopy)

if __name__ == '__main__':
  app.run( 
        host="0.0.0.0"
  )
